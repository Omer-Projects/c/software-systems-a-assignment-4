#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "graph.h"
#include "algo.h"

/**
 * @brief Entry point of application
 *
 * @return int error code
 */
int main()
{
    char action;
    node *head = NULL;

    while (scanf(" %c", &action) != -1)
    {
        switch (action)
        {
        case 'A':
        {
            if (head != NULL)
            {
                delete_graph(&head);
            }
            build_graph(&head);
        }
        break;
        case 'B':
        {
            if (head != NULL)
            {
                insert_node(&head);
            }
        }
        break;
        case 'D':
        {
            if (head != NULL)
            {
                delete_node(&head);
            }
        }
        break;
        case 'S':
        {
            if (head != NULL)
            {
                shorts_path(&head);
            }
        }
        break;
        case 'T':
        {
            if (head != NULL)
            {
                TSP(&head);
            }
        }
        break;
        }
    }

    delete_graph(&head);
    free(head);
}

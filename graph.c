#include <stdio.h>
#include <stdlib.h>

#include "graph.h"

// private
node *find_node(node **head, int node_num, node **node_before)
{
    node *n = *head;

    if (n->node_num == node_num)
    {
        *node_before = NULL;
        return n;
    }

    if (n->next == NULL)
    {
        *node_before = n;
        return NULL;
    }

    while (n->next->node_num < node_num && n->next->next != NULL)
    {
        n = n->next;
    }

    if (n->next->node_num == node_num)
    {
        *node_before = n;
        return n->next;
    }

    if (n->next->node_num < node_num)
    {

        *node_before = n->next;
        return NULL;
    }

    *node_before = n;
    return NULL;
}

// public
void build_graph(node **head)
{
    int n;
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
    {
        char temp;
        scanf(" %c", &temp);

        insert_node(head);
    }
}

void delete_graph(node **head)
{
    node *n = *head;
    while (n != NULL)
    {
        edge *e = n->edges;
        while (e != NULL)
        {
            edge *nextEdge = e->next;
            free(e);
            e = nextEdge;
        }

        node *nextNode = n->next;
        free(n);
        n = nextNode;
    }
    *head = NULL;
}

void insert_node(node **head)
{
    // find the node or create if not exists
    int node_num;
    scanf("%d", &node_num);

    node *new_node = NULL;

    if (*head == NULL)
    {
        new_node = (node *)malloc(sizeof(node));
        new_node->node_num = node_num;
        new_node->edges = NULL;
        new_node->next = NULL;

        *head = new_node;
    }
    else
    {
        node *node_before;
        new_node = find_node(head, node_num, &node_before);

        if (new_node == NULL)
        {
            // new node
            new_node = (node *)malloc(sizeof(node));
            new_node->node_num = node_num;
            new_node->edges = NULL;

            new_node->next = node_before->next;
            node_before->next = new_node;
        }
    }

    // remove the edges if exists
    if (new_node->edges != NULL)
    {
        edge *e = new_node->edges;
        while (e != NULL)
        {
            edge *nextEdge = e->next;
            free(e);
            e = nextEdge;
        }

        new_node->edges = NULL;
    }

    // add the new edges
    int endpoint_num;
    int weight;

    while (scanf("%d %d", &endpoint_num, &weight) == 2)
    {
        edge *new_edge = (edge *)malloc(sizeof(edge));
        new_edge->weight = weight;

        node *before_endpoint;
        node *endpoint = find_node(head, endpoint_num, &before_endpoint);

        if (endpoint == NULL)
        {
            // new node
            endpoint = (node *)malloc(sizeof(node));
            endpoint->node_num = endpoint_num;
            endpoint->edges = NULL;

            endpoint->next = before_endpoint->next;
            before_endpoint->next = endpoint;
        }

        new_edge->endpoint = endpoint;

        if (new_node->edges == NULL)
        {
            new_node->edges = new_edge;
            new_edge->next = NULL;
        }
        else if (new_node->edges->endpoint->node_num > endpoint_num)
        {
            new_edge->next = new_node->edges;
            new_node->edges = new_edge;
        }
        else if (new_node->edges->next == NULL)
        {
            new_node->edges->next = new_edge;
            new_edge->next = NULL;
        }
        else
        {
            edge *e = new_node->edges;

            while (e->next->endpoint->node_num < endpoint_num && e->next->next != NULL)
            {
                e = e->next;
            }

            if (e->next->endpoint->node_num > endpoint_num)
            {
                new_edge->next = e->next;
                e->next = new_edge;
            }
            else
            {
                e->next->next = new_edge;
                new_edge->next = NULL;
            }
        }
    }
}

void delete_node(node **head)
{
    int node_num;
    scanf("%d", &node_num);

    node *node_before;
    node *removing_node = find_node(head, node_num, &node_before);

    if (removing_node == NULL)
    {
        // the node not found
        return;
    }

    if (removing_node == *head)
    {
        *head = removing_node->next;

        if (*head == NULL)
        {
            // is the last node, so it can hold only edge for itself
            free(removing_node->edges);

            return;
        }
    }
    else
    {
        node_before->next = removing_node->next;
    }

    // remove the edges from the other nodes
    node *n = *head;
    while (n != NULL)
    {
        if (n->edges != NULL)
        {
            edge *e = n->edges;
            if (e->endpoint == removing_node)
            {
                n->edges = e->next;
                free(e);
            }
            else
            {
                while (e->next != NULL && e->next->endpoint < removing_node)
                {
                    e = e->next;
                }

                if (e->next != NULL && e->next->endpoint == removing_node)
                {
                    edge *temp = e->next;
                    e->next = e->next->next;
                    free(temp);
                }
            }
        }
        n = n->next;
    }

    // remove the node
    edge *e = removing_node->edges;

    while (e != NULL)
    {
        edge *next = e->next;
        free(e);
        e = next;
    }

    free(removing_node);
}

void print_graph(node **head)
{
    printf("Graph [\n");
    node *n = *head;
    while (n != NULL)
    {
        int node_num = n->node_num;
        printf("%d: ", node_num);
        edge *e = n->edges;
        while (e != NULL)
        {
            printf("w(%d, %d)=%d | ", node_num, e->endpoint->node_num, e->weight);

            e = e->next;
        }
        printf("\n");
        n = n->next;
    }
    printf("]\n");
}

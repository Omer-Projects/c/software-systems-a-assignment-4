# Project Path -
PROJECT_PATH=.

# Compiler prefix
CC=gcc -Wall

# clean, fixers and lintters
clean:
	rm -rf *.o graph

# code units
graph.o: $(PROJECT_PATH)/graph.c $(PROJECT_PATH)/graph.h
	$(CC) -c "$(PROJECT_PATH)/graph.c" -o graph.o

algo.o: $(PROJECT_PATH)/algo.c $(PROJECT_PATH)/algo.h $(PROJECT_PATH)/graph.h
	$(CC) -c "$(PROJECT_PATH)/algo.c" -o algo.o


main.o: $(PROJECT_PATH)/main.c $(PROJECT_PATH)/graph.h $(PROJECT_PATH)/algo.h
	$(CC) -c "$(PROJECT_PATH)/main.c" -o main.o

# applications
graph: main.o graph.o algo.o
	$(CC) main.o graph.o algo.o -o graph

# prod
all: graph

rebuild: clean all

# testing
test: rebuild
	./graph < "$(PROJECT_PATH)/inputs/input1.txt"
	./graph < "$(PROJECT_PATH)/inputs/input2.txt"
	./graph < "$(PROJECT_PATH)/inputs/input3.txt"
	./graph < "$(PROJECT_PATH)/inputs/input4.txt"
	./graph < "$(PROJECT_PATH)/inputs/input5.txt"

test-1: rebuild
	./graph < "$(PROJECT_PATH)/inputs/input1.txt"

test-2: rebuild
	./graph < "$(PROJECT_PATH)/inputs/input2.txt"

test-3: rebuild
	./graph < "$(PROJECT_PATH)/inputs/input3.txt"

test-4: rebuild
	./graph < "$(PROJECT_PATH)/inputs/input4.txt"

test-5: rebuild
	./graph < "$(PROJECT_PATH)/inputs/input5.txt"
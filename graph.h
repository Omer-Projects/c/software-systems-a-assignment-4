#pragma once

typedef struct _node node;
typedef struct _edge edge;

/**
 * @brief the node in graph
 */
struct _node
{
    int node_num;
    edge *edges;
    node *next; // The next node added to the graph
};

/**
 * @brief a edge between nodes in graph
 */
struct _edge
{
    int weight;
    node *endpoint;
    edge *next; // The next edge added to the node
};

void build_graph(node **head);
void delete_graph(node **head);
void print_graph(node **head);

void insert_node(node **head);
void delete_node(node **head);

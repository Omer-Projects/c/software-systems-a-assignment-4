#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "algo.h"

// private
int min_distance(int dist[], int sptSet[], int size)
{
    int min = -1;
    int min_index = -1;
    for (int v = 0; v < size; v++)
    {
        if (sptSet[v] == 0 && dist[v] != -1 && (min == -1 || dist[v] <= min))
        {
            min = dist[v];
            min_index = v;
        }
    }

    return min_index;
}

void dijkstra(node **head, int size, int *dist, int *sptSet, node *node_s)
{
    node *n = *head;

    for (int i = 0; i < size; i++)
    {
        sptSet[i] = 0;

        if (n == node_s)
        {
            dist[i] = 0;
        }
        else
        {
            dist[i] = -1;
        }

        n = n->next;
    }

    int u = 0;
    for (int i = 0; i < size - 1 && u != -1; i++)
    {
        u = min_distance(dist, sptSet, size);
        if (u != -1)
        {
            sptSet[u] = 1;

            if (dist[u] != -1)
            {
                // find u
                n = *head;
                for (size_t i = 0; i < u; i++)
                {
                    n = n->next;
                }

                node *node_u = n;

                edge *e = node_u->edges;
                n = *head;
                int v = 0;
                while (e != NULL)
                {
                    while (e->endpoint != n)
                    {
                        n = n->next;
                        v++;
                    }

                    if (sptSet[v] == 0 && (dist[u] + e->weight < dist[v] || dist[v] == -1))
                    {
                        dist[v] = dist[u] + e->weight;
                    }

                    e = e->next;
                }
            }
        }
    }
}

int get_path_weight(int **weights, int size, int *path)
{
    int path_weight = 0;
    int w;
    for (int i = 0; i < size - 1; i++)
    {
        w = weights[path[i]][path[i + 1]];
        if (w == -1)
        {
            return -1;
        }

        path_weight += w;
    }

    return path_weight;
}

int origin_tsp_path(int **weights, int size, int *path, int *in_path)
{
    int min_weight = -1;

    // reset path
    for (int i = 0; i < size; i++)
    {
        path[i] = i;
    }

    // run over the permutaions
    int k = size;
    while (k >= 0)
    {
        int now_weight = get_path_weight(weights, size, path);

        if (now_weight != -1 && (now_weight < min_weight || min_weight == -1))
        {
            min_weight = now_weight;
        }

        int good = 0;

        while (k >= 0 && good == 0)
        {
            k = size - 1;
            while (k >= 0 && path[k] == size - 1)
            {
                path[k] = 0;
                k--;
            }

            if (k != -1)
            {
                path[k]++;

                good = 1;

                memset(in_path, 0, size * sizeof(int));

                for (int i = 0; i < size; i++)
                {
                    in_path[path[i]]++;
                }

                for (int i = 0; i < size && good == 1; i++)
                {
                    if (in_path[i] != 1)
                    {
                        good = 0;
                    }
                }
            }
        }
    }

    return min_weight;
}

// public
void shorts_path(node **head)
{
    // get the nodes numbers
    int node_num_s;
    int node_num_t;
    scanf("%d %d", &node_num_s, &node_num_t);

    // find the nodes and the size of the graph
    node *node_s = NULL;
    node *node_t = NULL;

    node *n = *head;
    int size = 0;
    while (n != NULL)
    {
        if (n->node_num == node_num_s)
        {
            node_s = n;
        }

        if (n->node_num == node_num_t)
        {
            node_t = n;
        }

        size++;

        n = n->next;
    }

    if (node_s == NULL || node_t == NULL)
    {
        // at last one of the nodes not found
        printf("TSP shortest path: -1 \n");
        return;
    }

    // running dijkstra
    int *dist = (int *)malloc(size * sizeof(int));
    int *sptSet = (int *)malloc(size * sizeof(int));

    dijkstra(head, size, dist, sptSet, node_s);

    // find index of node_t
    n = *head;

    int index_t = 0;
    while (n != node_t)
    {
        n = n->next;
        index_t++;
    }

    printf("Dijsktra shortest path: %d \n", dist[index_t]);

    free(dist);
    free(sptSet);
}

void TSP(node **head)
{
    // get full size
    int size = 0;
    node *n = *head;
    while (n != NULL)
    {
        size++;
        n = n->next;
    }

    int *k_nodes = (int *)malloc(size * sizeof(int));
    int *dist = (int *)malloc(size * sizeof(int));
    int *sptSet = (int *)malloc(size * sizeof(int));

    memset(k_nodes, 0, size * sizeof(int));

    // get the k nodes
    int k;
    scanf("%d", &k);
    for (int i = 0; i < k; i++)
    {
        int node_i_num;
        scanf("%d", &node_i_num);

        n = *head;
        int index = 0;
        while (n->node_num < node_i_num && n->next != NULL)
        {
            index++;
            n = n->next;
        }

        if (n == NULL || n->node_num != node_i_num)
        {
            printf("ERROR: TSP 1\n");
            return;
        }

        k_nodes[index]++;

        if (k_nodes[index] > 1)
        {
            printf("ERROR: TSP 2\n");
            return;
        }
    }

    // get the shortests path between the k nodes
    int **weights = (int **)malloc(k * sizeof(int *));
    for (int i = 0; i < k; i++)
    {
        weights[i] = (int *)malloc(k * sizeof(int));
    }

    // by k * dijkstra
    node *node_s = *head;
    int node_index = 0;
    for (int i = 0; i < k; i++)
    {
        while (k_nodes[node_index] == 0)
        {
            node_s = node_s->next;
            node_index++;
        }

        dijkstra(head, size, dist, sptSet, node_s);

        int k_index = 0;
        for (int j = 0; j < k; j++)
        {
            while (k_nodes[k_index] == 0)
            {
                k_index++;
            }

            weights[i][j] = dist[k_index];
            k_index++;
        }

        node_s = node_s->next;
        node_index++;
    }

    // running "origin shortest" path on the smaller graph
    int cost = origin_tsp_path(weights, k, dist, sptSet);

    // print the shortest
    printf("TSP shortest path: %d \n", cost);

    // cleanup
    free(k_nodes);
    free(dist);
    free(sptSet);

    for (int i = 0; i < k; i++)
    {
        free(weights[i]);
    }
    free(weights);
}
